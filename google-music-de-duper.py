#!/usr/bin/env python3

import re
import sys
import time

from gmusicapi import Mobileclient
from unidecode import unidecode

def slugify(string):
    return re.sub('[^a-z0-9]+','-', unidecode(string).lower().replace('&', 'and'))

def get_tracks_by_album(tracks):
    tracks_by_album = {}
    for track in tracks:
        album = track['album'].lower()
        if album not in tracks_by_album:
            tracks_by_album[album] = []
        tracks_by_album[album].append(track)
    return tracks_by_album

def get_track_map(tracks):
    track_map = {}
    for track in tracks:
        album = slugify(track['album'])
        title = slugify(track['title'])
        artist = slugify(track['artist'] if 'artist' in track else track['albumArtist'] if 'albumArtist' in track else 'Unknown')

        key = "%s-%s-%s" % (artist, album, title)
        if not key in track_map:
            track_map[key] = []
        track_map[key].append(track)
    return track_map

def get_duplicate_tracks(tracks):
    track_map = get_track_map(tracks)
    tracks_with_dupes = [ tracks for _, tracks in track_map.items() if len(tracks) > 1 ]
    # Now we're removing the earliest track
    # This is modifying in-place, so there's no need to get the returned list
    [ tracks.remove(min(tracks, key = lambda track: int(track['creationTimestamp']))) for tracks in tracks_with_dupes ]
    return [ track for track_list in tracks_with_dupes for track in track_list ]

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is one of "yes" or "no".
    """
    valid = {"yes": True,   "y": True,  "ye": True,
             "no": False,     "n": False}
    if default == None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        if sys.version_info[0] < 3:
            choice = raw_input().lower()
        else:
            choice = input().lower()

        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def clean_duplicates(tracks):
    if query_yes_no("Found " + str(len(tracks)) + " duplicate tracks. Delete duplicates?", "no"):
        deleted_track_ids = []
        for track in tracks:
            time.sleep(1)
            deleted_track_ids += api.delete_songs(track['id'])
            print("Deleted duplicate %s by %s" % (track['title'], track['artist'] if 'artist' in track else 'Unknown'))
        print("Deleted %d tracks" % len(deleted_track_ids))

if __name__ == '__main__':
    api = Mobileclient()
    try:
        api.oauth_login(Mobileclient.FROM_MAC_ADDRESS)
    except:
        print("No OAuth credentials found! Please setup in the following screen!")
        api.perform_oauth()
        api.oauth_login(Mobileclient.FROM_MAC_ADDRESS) # If it fails here, it wasn't meant to be

    if api.is_authenticated():
        print("Successfully logged in. Beginning duplicate detection process.")
        all_tracks = api.get_all_songs()
        tracks_by_album = get_tracks_by_album(all_tracks)
        for album in tracks_by_album:
            print("Checking %s for duplicates" % album)
            dupes = get_duplicate_tracks(tracks_by_album[album])
            clean_duplicates(dupes) if len(dupes) > 0 else print("No duplicates found in %s" % album)
        print("All done!")
    else:
        print("Failed to authenticate. Bye!")
