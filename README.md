# Google Music Deduper

This tool is for removing duplicate songs from your Google Music Collection.

I find that every time I use Google Music Manager on my desktop, a bunch of duplicate songs show up.

Most of this code is borrowed from https://github.com/maxkirchoff/google-music-dupe-killer and
https://github.com/kennyszub/google-music-playlist-scripts

I combined some of their ideas, and cleaned up acording to my tastes.

To run this, you'll need gmusicapi and unidecode, both of which can be installed through `pip`.

```shell
pip install gmusicapi unidecode
```

After that, simply run `python google-music-de-duper.py` and follow the instructions. It will ask you before deleting
anything, and tell you which album it's deleting from. When you first run it, it will probably ask you to follow a link
and log in to your Google account. These credentials are stored on your computer and can be reused by any python app
using the `gmusicapi` library.
